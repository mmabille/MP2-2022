package ch.epfl.cs107.play.game.icrogue;

import ch.epfl.cs107.play.game.areagame.Area;
import ch.epfl.cs107.play.game.areagame.AreaGame;
import ch.epfl.cs107.play.game.areagame.actor.Orientation;
import ch.epfl.cs107.play.game.icrogue.actors.ICRoguePlayer;
import ch.epfl.cs107.play.game.icrogue.area.ICRogueRoom;
import ch.epfl.cs107.play.game.icrogue.area.level0.rooms.Level0Room;

import ch.epfl.cs107.play.game.tutosSolution.actor.GhostPlayer;
import ch.epfl.cs107.play.io.FileSystem;
import ch.epfl.cs107.play.math.DiscreteCoordinates;
import ch.epfl.cs107.play.window.Window;

public class ICRogue extends AreaGame {

    public String getTitle() {
        return "ICRogue";
    }
    private ICRoguePlayer player;
    public final static float CAMERA_SCALE_FACTOR = 13.f;

    private Level0Room salleCourante;
    private int areaIndex;

    private void initLevel(){

        salleCourante = new Level0Room(new DiscreteCoordinates(0,0));
        addArea(salleCourante);
        ICRogueRoom area = (ICRogueRoom) setCurrentArea(salleCourante.getTitle(), true);
        DiscreteCoordinates coords = area.getPlayerSpawnPosition();
        player = new ICRoguePlayer(salleCourante, Orientation.DOWN, new DiscreteCoordinates(2,2), "zelda/player");
        player.enterArea(salleCourante, coords);

        }
    private final String[] areas = {};


    @Override
    public boolean begin(Window window, FileSystem fileSystem) {

        if (super.begin(window, fileSystem)) {
            initLevel();
            //areaIndex = 0;
            //initArea(areas[areaIndex]);
            return true;
        }
        return false;
    }

    private void initArea(String areaKey) {

        ICRogueRoom area = (ICRogueRoom) setCurrentArea(areaKey, true);
        DiscreteCoordinates coords = area.getPlayerSpawnPosition();
        //player = new ICRoguePlayer();


    }
    @Override
    public void update(float deltaTime) {
        //if(player.isWeak()){
         //   switchArea();
        //}
        super.update(deltaTime);

    }

    @Override
    public void end() {
    }


    protected void switchArea() {

        //player.leaveArea();

        areaIndex = (areaIndex==0) ? 1 : 0;

        ICRogueRoom currentArea = (ICRogueRoom) setCurrentArea(areas[areaIndex], false);
        //player.enterArea(currentArea, currentArea.getPlayerSpawnPosition());

        //player.strengthen();
    }


    }

