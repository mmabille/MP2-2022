package ch.epfl.cs107.play.game.icrogue.area.level0.rooms;

import ch.epfl.cs107.play.game.icrogue.area.ICRogueRoom;
import ch.epfl.cs107.play.math.DiscreteCoordinates;
import ch.epfl.cs107.play.game.areagame.actor.Background;

public class Level0Room extends ICRogueRoom {

    private static String behaviorName = "icrogue/Level0Room";

    public Level0Room(DiscreteCoordinates roomCoordinates) {
        super(roomCoordinates, behaviorName);
    }


    @Override
    public String getTitle() {
        return "icrogue/level0"+getRoomCoordinates();
    }


    @Override
    protected void createArea() {
        registerActor(new Background(this, behaviorName));
    }

    @Override
    public DiscreteCoordinates getPlayerSpawnPosition() {
        return new DiscreteCoordinates(2,2);
    }
}
